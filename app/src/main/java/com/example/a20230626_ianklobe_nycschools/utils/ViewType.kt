package com.example.a20230626_ianklobe_nycschools.utils

import com.example.a20230626_ianklobe_nycschools.view.adapter.SchoolsDataAdapter

sealed class ViewType{

    data class LETTER(val letter: String): ViewType()
    data class SCHOOLS_DATA(val schoolsData: SchoolsDataAdapter): ViewType()

}
