package com.example.a20230626_ianklobe_nycschools.utils

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.a20230626_ianklobe_nycschools.di.SchoolsApp
import com.example.a20230626_ianklobe_nycschools.viewModel.SchoolsViewModel
import com.example.a20230626_ianklobe_nycschools.utils.SchoolsViewModelFactory
import javax.inject.Inject

open class BaseFragment : Fragment() {

    @Inject
    lateinit var schoolsViewModelFactory: SchoolsViewModelFactory
    var currentDbn = ""

    protected val schoolsViewModel: SchoolsViewModel by lazy {
        ViewModelProvider(requireActivity(), schoolsViewModelFactory)[SchoolsViewModel::class.java]
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SchoolsApp.schoolsComponent.inject(this)
    }

}