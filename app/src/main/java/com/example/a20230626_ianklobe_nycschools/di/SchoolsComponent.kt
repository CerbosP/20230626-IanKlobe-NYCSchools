package com.example.a20230626_ianklobe_nycschools.di

import com.example.a20230626_ianklobe_nycschools.MainActivity
import com.example.a20230626_ianklobe_nycschools.utils.BaseFragment
import com.example.a20230626_ianklobe_nycschools.di.RepositoryModule
import dagger.Component

/**
 * [Interface] - specifies the Dependency Injection components
 */

@Component(modules = [
    NetworkModule::class,
    RepositoryModule::class,
    ApplicationModule::class
])
interface SchoolsComponent {

    /**
     * Method to inject dependencies in the Main Activity
     */
    fun inject(mainActivity: MainActivity)

    /**
     * Method to inject dependencies in the Base Fragment
     */
    fun inject(baseFragment: BaseFragment)

}