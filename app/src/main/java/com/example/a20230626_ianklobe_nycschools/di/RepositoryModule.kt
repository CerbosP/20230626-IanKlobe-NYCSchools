package com.example.a20230626_ianklobe_nycschools.di

import com.example.a20230626_ianklobe_nycschools.rest.SchoolsRepository
import com.example.a20230626_ianklobe_nycschools.rest.SchoolsRepositoryImpl
import dagger.Binds
import dagger.Module

/**
 * [Abstract Class] - Module for repository
 */

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun provideSchoolsRepository(
        schoolsRepositoryImpl: SchoolsRepositoryImpl
    ): SchoolsRepository

}